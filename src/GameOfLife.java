import java.util.Random;

/**
 * Created by Евгений on 29.06.2017.
 */
public class GameOfLife {
    public static void main(String[] args){

        firstRandom();
    }

    public static void firstRandom(){
        int[][] arr = new int[50][50];
        Random random = new Random();
        for (int i=0;i < arr.length;i++) {
            for (int j=0;j < arr[i].length;j++) {
                arr[i][j]=random.nextInt(2);
            }
        }
        for (int i=0;i < arr.length;i++){
            System.out.println();
            for (int j=0;j < arr[i].length;j++) {
                switch (arr[i][j]){
                    case 0:
                        System.out.print(" ");
                        break;
                    case 1:
                        System.out.print("X");
                        break;
                    default:
                        System.out.print("Somewhere I blunted!:(");
                }


            }
        }

    }

}
